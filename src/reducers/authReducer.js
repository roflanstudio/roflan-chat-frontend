import * as types from "../actions/constants";

const INITIAL_STATE = {
    email: "",
    token: "",
};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case types.LOGIN_SUCCESS:
            localStorage.setItem("token", action.payload.data.token);
            return {
                ...state,
                token: action.payload.data.token,
            };
        default:
            return state;
    }
}
