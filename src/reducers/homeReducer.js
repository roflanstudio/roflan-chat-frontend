import * as types from "../actions/constants";

const INITIAL_STATE = {
    info: {},
};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case types.GET_INFO_SUCCESS:
            return {
                ...state,
                info: action.payload.data,
            };
        default:
            return state;
    }
}
