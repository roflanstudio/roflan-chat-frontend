import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import { reducer as formReducer } from "redux-form";
import AuthReducer from "./authReducer";
import HomeReducer from "./homeReducer";
import ChatReducer from "./allChatReducer";

const rootReducer = (history) =>
    combineReducers({
        router: connectRouter(history),
        form: formReducer,
        auth: AuthReducer,
        home: HomeReducer,
        chat: ChatReducer
    });

export default rootReducer;
