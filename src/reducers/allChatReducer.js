import * as types from "../actions/constants";

const INITIAL_STATE = {
    history: [],
};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case types.GET_HISTORY_ALL_CHAT_SUCCESS:
            return {
                ...state,
                history: action.payload.data,
            };
        default:
            return state;
    }
}
