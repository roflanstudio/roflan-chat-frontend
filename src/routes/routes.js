import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import App from "../containers/App";
import TestSockJs from "../components/TestSockJs/TestSockJs";
import AuthContainer from "./../containers/AuthContainer";
import Message from "./../components/HelperComponents/Message/Message";
import MyEditor from "./../components/HelperComponents/Editor/Editor";
import RegisterConfirmed from "../components/Auth/Register/RegisterConfirmed";
import RegisterConfirmedChange from "../components/Auth/PasswordRecovery/PasswordRecoveryChange";
import AnotherOneEditor from "./../components/HelperComponents/AnotherOneEditor/AnotherOneEditor";
import MainChat from "./../components/MainChat/MainChat";
import Layout from "./../containers/MainContainer";

const routes = (
    <App>
        <Switch>
            <Route
                path="/"
                exact
                render={() =>
                    !!localStorage.token ? (
                        <Redirect to="/main/home" />
                    ) : (
                        <Redirect to="/auth/login" />
                    )
                }
            />
            <Route path="/main" component={Layout} />
            <Route path="/auth" component={AuthContainer} />
            <Route path="/verify" component={RegisterConfirmed} />
            <Route path="/reset" component={RegisterConfirmedChange} />
            <Route render={() => <div>404</div>} />
        </Switch>
        <ToastContainer />
    </App>
);

export default routes;
