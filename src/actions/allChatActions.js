import * as types from "./constants.js";

export function getAllChatHistory() {
    return {
        type: types.GET_HISTORY_ALL_CHAT,
        payload: {
            client: "default",
            request: {
                url: `getLogs`,
                method: "get",
            },
        },
    };
}
