import * as types from "./constants.js";

export function register(data) {
    return {
        type: types.REGISTER,
        payload: {
            client: "default",
            request: {
                url: `registration`,
                method: "post",
                data,
            },
        },
    };
}

export function postLogin(data) {
    return {
        type: types.LOGIN,
        payload: {
            client: "default",
            request: {
                url: `login`,
                method: "post",
                data,
            },
        },
    };
}

export function verifyEmail(data) {
    return {
        type: types.VERIFY_EMAIL,
        payload: {
            client: "default",
            request: {
                url: `registrationConfirm?token=${data.token}`,
                method: "get",
                data,
            },
        },
    };
}

export function recoveryPasswordSent(data) {
    return {
        type: types.RECOVERY_PASSWORD_SENT,
        payload: {
            client: "default",
            request: {
                url: `passwordReset?email=${data.email}`,
                method: "post",
            },
        },
    };
}

export function recoveryPasswordChange(token) {
    return {
        type: types.RECOVERY_PASSWORD_CHANGE,
        payload: {
            client: "default",
            request: {
                url: `passwordReset?token=${token}`,
                method: "put",
            },
        },
    };
}