import * as types from "./constants.js";

export function getInfo() {
    return {
        type: types.GET_INFO,
        payload: {
            client: "default",
            request: {
                url: `user/info`,
                method: "get",
            },
        },
    };
}
