//////////////////////////////auth actions//////////////////////////////

export const LOGIN = "LOGIN";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";

export const REGISTER = "REGISTER";
export const REGISTER_FAIL = "REGISTER_FAIL";

export const VERIFY_EMAIL = "VERIFY_EMAIL";

export const RECOVERY_PASSWORD_SENT = "RECOVERY_PASSWORD_SENT";
export const RECOVERY_PASSWORD_CHANGE = "RECOVERY_PASSWORD_CHANGE";

/////////////////////////////home actions//////////////////////////////

export const GET_INFO = "GET_INFO";
export const GET_INFO_SUCCESS = "GET_INFO_SUCCESS";

/////////////////////////////all chat actions//////////////////////

export const GET_HISTORY_ALL_CHAT = "GET_HISTORY_ALL_CHAT";
export const GET_HISTORY_ALL_CHAT_SUCCESS = "GET_HISTORY_ALL_CHAT_SUCCESS";
