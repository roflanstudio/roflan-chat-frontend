import React from "react";
import { Switch, Route, Redirect, useRouteMatch } from "react-router-dom";

import Login from "../components/Auth/Login/Login";
import Register from "../components/Auth/Register/Register";
import PasswordRecovery from './../components/Auth/PasswordRecovery/PasswordRecovery';
import PasswordRecoverySent from './../components/Auth/PasswordRecovery/PasswordRecoverySent';

const AuthContainer = () => {
    const { path } = useRouteMatch();
    return !!localStorage.token ? (
        <Redirect to="/main" />
    ) : (
        <Switch>
            <Route path={`${path}/login`} exact component={Login} />
            <Route path={`${path}/register`} exact component={Register} />
            <Route path={`${path}/recovery`} exact component={PasswordRecovery} />
            <Route path={`${path}/recovery-sent`} exact component={PasswordRecoverySent} />
        </Switch>
    );
};

export default AuthContainer;
