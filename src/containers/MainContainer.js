import React, { useEffect, useState } from "react";
import { Route, Switch, useRouteMatch } from "react-router-dom";
import { connect } from "react-redux";
import "./Containers.scss";
import { getInfo } from "./../actions/homeActions";
import MainChat from './../components/MainChat/MainChat';

const Layout = ({ history, getInfo }) => {
    useEffect(() => {
        !(localStorage.token || localStorage.admin_token) &&
            history.push("/auth/login/");
        getInfo().then((res) => {
            if (
                res.payload &&
                res.payload.status &&
                res.payload.status === 200
            ) {
                console.log(res)
            }
        });
    }, []);

    const { path } = useRouteMatch();
    return (
        <div className="layout">
            <Switch>
                <Route exact path={`${path}`} component={MainChat} />
            </Switch>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {};
};

const mapDispatchToProps = {
    getInfo,
};

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
