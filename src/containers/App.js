import React, { Component } from "react";
import "../style/main.scss";
import { Context as ResponsiveContext } from "react-responsive";

class App extends Component {

    render() {
        return (
            <ResponsiveContext.Provider>
                {this.props.children}
            </ResponsiveContext.Provider>
        );
    }
}

export default App;
