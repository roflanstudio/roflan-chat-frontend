let BASE_URL, SECOND_URL;

if (window.location.host === "localhost:3000") {
    BASE_URL = "https://roflan-chat-backend.herokuapp.com/";
} else if (localStorage.getItem("BASE_URL")) {
    BASE_URL = localStorage.getItem("BASE_URL");
} else {
    BASE_URL = "https://roflan-chat-backend.herokuapp.com/";
}

SECOND_URL = "";

export const API_BASE_URL = BASE_URL;
export const API_SECOND_URL = SECOND_URL;
