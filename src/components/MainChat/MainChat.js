import React, { useState, useEffect } from "react";
import ChatBody from "./../HelperComponents/ChatBody/ChatBody";
import { connect } from "react-redux";
import { getAllChatHistory } from "../../actions/allChatActions";

const MainChat = ({ messages, getAllChatHistory }) => {
    useEffect(() => {
        getAllChatHistory().then((res) => console.log(res));
    }, []);
    return <ChatBody messages={[]} />;
};

const mapStateToProps = (state) => {
    return {
        messages: state.chat.history,
    };
};

const mapDispatchToProps = {
    getAllChatHistory,
};

export default connect(mapStateToProps, mapDispatchToProps)(MainChat);
