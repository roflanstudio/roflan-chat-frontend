import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { getInfo } from "./../../../actions/homeActions";
import EditorJs from "react-editor-js";
import { EDITOR_JS_TOOLS } from "./../../../helpers/constants";

const AnotherOneEditor = ({ getInfo }) => {
    useEffect(() => {
        getInfo();
    }, []);

    return (
        <EditorJs
            tools={EDITOR_JS_TOOLS}
            data={{
                time: 1556098174501,
                blocks: [
                    {
                        type: "paragraph",
                        data: {
                            text:
                                "We have been working on this project more than three years. Several large media projects help us to test and debug the Editor, to make it's core more stable. At the same time we significantly improved the API. Now, it can be used to create any plugin for any task. Hope you enjoy. 😏",
                        },
                    },
                ],
            }}
        />
    );
};

const mapDispatchToProps = {
    getInfo,
};

export default connect(null, mapDispatchToProps)(AnotherOneEditor);
