import React from "react";
import "./Message.scss";

const Message = ({ message, author, ownMessage }) => {
    return (
        <div
            className={ownMessage ? "message-wrapper right" : "message-wrapper"}
        >
            <div className={ownMessage ? "own-message" : "message"}>
                <div className={"message-author"}>{author}</div>
                <div className={"message-text"}>{message}</div>
            </div>
        </div>
    );
};

export default Message;
