import React, { useState, useEffect } from "react";
import { Editor } from "react-draft-wysiwyg";
import { EditorState, convertToRaw } from "draft-js";
import "../../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import draftToHtml from "draftjs-to-html";
import { connect } from "react-redux";
import { getInfo } from "./../../../actions/homeActions";

const MyEditor = ({ getInfo }) => {
    useEffect(() => {
        getInfo();
    }, []);
    const [editorState, setEditorState] = useState(() =>
        EditorState.createEmpty()
    );
    console.log(draftToHtml(convertToRaw(editorState.getCurrentContent())));
    return (
        <Editor
            editorState={editorState}
            toolbarClassName="demo-toolbar-custom"
            wrapperClassName="demo-wrapper"
            editorClassName="demo-editor-custom"
            onEditorStateChange={(e) => setEditorState(e)}
        />
    );
};

const mapDispatchToProps = {
    getInfo,
};

export default connect(null, mapDispatchToProps)(MyEditor);
