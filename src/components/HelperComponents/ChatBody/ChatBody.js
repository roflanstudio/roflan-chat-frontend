import React from "react";
import Message from "./../Message/Message";
import "./ChatBody.scss";

const ChatBody = ({ messages }) => {
    return (
        <div className="chat-body">
            {messages.map((el) => (
                <Message />
            ))}
        </div>
    );
};

export default ChatBody;
