import React from "react";
import SockJsClient from "react-stomp";
import { connect } from "react-redux";
import { getInfo } from "../../actions/homeActions";
import { getAllChatHistory } from "../../actions/allChatActions";

class SampleComponent extends React.Component {
    componentDidMount() {
        this.props.getInfo();
        this.props.getAllChatHistory();
    }

    sendMessage = (message) => {
        const { info } = this.props;
        this.clientRef.sendMessage(
            "/app/chat",
            JSON.stringify({ from: info && info.email, message: message })
        );
    };

    render() {
        return (
            <div>
                <div onClick={() => this.sendMessage("123")}>123</div>
                <SockJsClient
                    url="https://roflan-chat-backend.herokuapp.com/ws"
                    topics={["/topic/messages"]}
                    subscribeHeaders={{
                        Authorization: `Bearer ${localStorage.getItem(
                            "token"
                        )}`,
                    }}
                    headers={{
                        Authorization: `Bearer ${localStorage.getItem(
                            "token"
                        )}`,
                    }}
                    onMessage={(msg) => {
                        console.log(msg);
                    }}
                    ref={(client) => {
                        this.clientRef = client;
                    }}
                />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        info: state.home.info,
    };
};

const mapDispatchToProps = {
    getInfo,
    getAllChatHistory
};

export default connect(mapStateToProps, mapDispatchToProps)(SampleComponent);
