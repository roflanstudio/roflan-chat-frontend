import React, { Component } from "react";
import "../Register/Register.scss";
import { Link } from "react-router-dom";

class PasswordRecoverySent extends Component {
    render() {
        return (
            <div className="auth-background">
                <div className="register">
                    <h1 className="register-title">Instructions sent!</h1>
                    <p
                        className="register-login-text"
                        style={{ marginTop: "10px" }}
                    >
                        You’ll receive this email within 5 minutes. Be sure to
                        check your spam folder, too.
                    </p>
                    <div className="register-login">
                        <Link className="register-login-link" to="/auth/login">
                            Back to login
                        </Link>
                    </div>
                </div>
            </div>
        );
    }
}

export default PasswordRecoverySent;
