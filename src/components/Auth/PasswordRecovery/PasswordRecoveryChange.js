import React, { Component } from "react";
import "../Register/Register.scss";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { Field, reduxForm, SubmissionError } from "redux-form";
import RenderField from "../../HelperComponents/RenderField/RenderField";
import { recoveryPasswordChange } from "../../../actions/authActions";
import { toast } from "react-toastify";

class PasswordRecoveryChange extends Component {
    componentDidMount() {
        const { query, recoveryPasswordChange } = this.props;
        if (!("security_token" in query)) {
            this.setState({ tokenError: true });
        }
        recoveryPasswordChange(query?.code);
    }

    render() {
        return (
            <div className="auth-background">
                <div className="register">
                    <h1 className="register-title">Password Recovery</h1>
                    <p
                        className="register-login-text"
                        style={{ marginTop: "10px" }}
                    >
                        Thank you, your password has been successfully changed.
                        We have sent a new password to your email.
                    </p>
                    <div className="register-login">
                        <Link className="register-login-link" to="/auth/login">
                            Back to login
                        </Link>
                    </div>
                </div>
            </div>
        );
    }
}

const validate = (values) => {
    const errors = {};
    if (!values.password) {
        errors.password = "Required field";
    } else if (values.password.length < 8) {
        errors.password = "Password must contain 8 or more symbols";
    }
    if (!values.password_confirm) {
        errors.password_confirm = "Required field";
    } else if (values.password !== values.password_confirm) {
        errors.password_confirm = "Passwords are not the same";
    }
    return errors;
};

PasswordRecoveryChange = reduxForm({
    form: "PasswordRecoveryChangeForm",
    validate,
})(PasswordRecoveryChange);

const mapStateToProps = (state) => {
    return {
        query: state.router.location.query,
    };
};

const mapDispatchToProps = {
    recoveryPasswordChange,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PasswordRecoveryChange);
