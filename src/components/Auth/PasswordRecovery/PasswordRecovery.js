import React, { Component } from "react";
import "../Register/Register.scss";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { Field, reduxForm, SubmissionError } from "redux-form";
import RenderField from "../../HelperComponents/RenderField/RenderField";
import { recoveryPasswordSent } from "../../../actions/authActions";
import { toast } from "react-toastify";

class PasswordRecovery extends Component {
    submitForm = (data) => {
        console.log(data);
        const { recoveryPasswordSent, history } = this.props;
        return recoveryPasswordSent(data).then((res) => {
            console.log(res);
            if (
                res.payload &&
                res.payload.status &&
                res.payload.status === 200
            ) {
                // toast("You have registered successfully.");
                history.push("/auth/recovery-sent");
            } else {
                console.log(res);
                throw new SubmissionError({
                    _error: res.error.response.data.error,
                });
            }
        });
    };

    render() {
        const { handleSubmit, error } = this.props;
        return (
            <div className="auth-background">
                <div className="register">
                    <h1 className="register-title">Password Recovery</h1>
                    <form onSubmit={handleSubmit(this.submitForm)}>
                        <div className="register-input">
                            <Field
                                name="email"
                                type="email"
                                placeholder="Email"
                                component={RenderField}
                            />
                        </div>
                        {error && <span className="main-error">{error}</span>}
                        <button className="register-btn" type="submit">
                            Continue
                        </button>
                    </form>
                    <div className="register-login">
                        <Link className="register-login-link" to="/auth/login">
                            Back to login
                        </Link>
                    </div>
                </div>
            </div>
        );
    }
}

const validate = (values) => {
    const errors = {};
    if (!values.email) {
        errors.email = "Required field";
    } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,5}$/i.test(values.email)
    ) {
        errors.email = "Invalid email address";
    }
    return errors;
};

PasswordRecovery = reduxForm({
    form: "PasswordRecoveryForm",
    validate,
})(PasswordRecovery);

const mapDispatchToProps = {
    recoveryPasswordSent,
};

export default connect(null, mapDispatchToProps)(PasswordRecovery);
