import React, { Component } from "react";
import "./Register.scss";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { Field, reduxForm, SubmissionError } from "redux-form";
import RenderField from "../../HelperComponents/RenderField/RenderField";
import { register } from "../../../actions/authActions";
import { toast } from "react-toastify";

class Register extends Component {
    submitForm = (data) => {
        const { register, history } = this.props;
        return register(data).then((res) => {
            if (
                res.payload &&
                res.payload.status &&
                res.payload.status === 201
            ) {
                toast(`Instructions have sent to ${data.email}`);
                history.push("/auth/login");
            } else {
                console.log(res);
                throw new SubmissionError({
                    username:
                        res.error.response.data.username &&
                        res.error.response.data.username[0],
                    password:
                        res.error.response.data.password &&
                        res.error.response.data.password[0],
                    email:
                        res.error.response.data.email &&
                        res.error.response.data.email[0],
                    _error: res.error.response.data.error,
                });
            }
        });
    };

    render() {
        const { handleSubmit, error } = this.props;
        return (
            <div className="auth-background">
                <div className="register">
                    <h1 className="register-title">Register new account</h1>
                    <form onSubmit={handleSubmit(this.submitForm)}>
                        <div className="register-input">
                            <Field
                                name="username"
                                type="text"
                                placeholder="Full name"
                                component={RenderField}
                            />
                        </div>
                        <div className="register-input">
                            <Field
                                name="email"
                                type="email"
                                placeholder="Email"
                                component={RenderField}
                            />
                        </div>
                        <div className="register-input">
                            <Field
                                name="password"
                                type="password"
                                placeholder="Password"
                                component={RenderField}
                            />
                        </div>
                        <div className="register-input">
                            <Field
                                name="password_confirm"
                                type="password"
                                placeholder="Repeat password"
                                component={RenderField}
                            />
                        </div>
                        {error && <span className="main-error">{error}</span>}
                        <button className="register-btn" type="submit">
                            Sign Up
                        </button>
                    </form>

                    <div className="register-login">
                        <div className="register-login-text">
                            Do you have an account?
                        </div>
                        <Link className="register-login-link" to="/auth/login">
                            Sign in
                        </Link>
                    </div>
                </div>
            </div>
        );
    }
}

const validate = (values) => {
    const errors = {};
    if (!values.username) {
        errors.username = "Required field";
    }
    if (!values.email) {
        errors.email = "Required field";
    } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,5}$/i.test(values.email)
    ) {
        errors.email = "Invalid email address";
    }
    if (!values.password) {
        errors.password = "Required field";
    } else if (values.password.length < 8) {
        errors.password = "Password must contain 8 or more symbols";
    }
    if (!values.password_confirm) {
        errors.password_confirm = "Required field";
    } else if (values.password !== values.password_confirm) {
        errors.password_confirm = "Passwords are not the same";
    }
    return errors;
};

Register = reduxForm({
    form: "RegisterForm",
    validate,
})(Register);

const mapDispatchToProps = {
    register,
};

export default connect(null, mapDispatchToProps)(Register);
