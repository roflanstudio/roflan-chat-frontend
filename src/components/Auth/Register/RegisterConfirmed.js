import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { verifyEmail } from "./../../../actions/authActions";

const RegisterConfirmed = ({ query, verifyEmail }) => {
    const [error, setError] = useState(false);
    useEffect(() => {
        if ("code" in query)
            verifyEmail({ token: query.code }).then((res) => {
                if (res.type === "VERIFY_EMAIL_FAIL") setError(true);
                else localStorage.setItem("verified", "true");
            });
        else setError(true);
    }, [query]);

    return (
        <div className="auth-background">
            <div className="register">
                {!error ? (
                    <>
                        <h1
                            className="register-title"
                            style={{ fontSize: "32px" }}
                        >
                            Email address confirmed
                        </h1>
                        <p
                            className="register-login-text"
                            style={{ marginTop: "10px" }}
                        >
                            You have succesfully confirmed your email address.
                            Please use it to log in again.
                        </p>
                    </>
                ) : (
                    <h1 className="register-title">Error</h1>
                )}
                <Link
                    to="/auth/login"
                    className="register-btn"
                    style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                    }}
                >
                    Log In
                </Link>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        query: state.router.location.query,
    };
};

const mapDispatchToProps = {
    verifyEmail,
};

export default connect(mapStateToProps, mapDispatchToProps)(RegisterConfirmed);
