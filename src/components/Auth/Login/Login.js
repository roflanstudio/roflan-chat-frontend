import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { Field, reduxForm, SubmissionError } from "redux-form";
import RenderField from "../../HelperComponents/RenderField/RenderField";
import { postLogin } from "../../../actions/authActions";
import "../Register/Register.scss";

class Login extends Component {
    submitForm = (data) => {
        const { postLogin, history } = this.props;
        console.log(data);
        return postLogin(data).then((res) => {
            if (
                res.payload &&
                res.payload.status &&
                res.payload.status === 200
            ) {
                history.push("/main");
            } else {
                throw new SubmissionError({
                    username:
                        res.error.response.data.username &&
                        res.error.response.data.username[0],
                    password:
                        res.error.response.data.password &&
                        res.error.response.data.password[0],
                    email:
                        res.error.response.data.email &&
                        res.error.response.data.email[0],
                    _error: res.error.response.data.error,
                });
            }
        });
    };

    render() {
        const { handleSubmit, error } = this.props;
        return (
            <div className="auth-background">
                <div className="register">
                    <h1 className="register-title">Login to account</h1>
                    <form onSubmit={handleSubmit(this.submitForm)}>
                        <div className="register-input">
                            <Field
                                name="email"
                                type="email"
                                placeholder="Email"
                                component={RenderField}
                            />
                        </div>
                        <div className="register-input">
                            <Field
                                name="password"
                                type="password"
                                placeholder="Password"
                                component={RenderField}
                            />
                        </div>
                        <Link
                            className="register-login-link"
                            to="/auth/recovery"
                            className="auth-recovery"
                        >
                            Forgot Password?
                        </Link>
                        {error && <span className="main-error">{error}</span>}
                        <button className="register-btn" type="submit">
                            Sign In
                        </button>
                    </form>

                    <div className="register-login">
                        <div className="register-login-text">
                            Don’t have an account?
                        </div>
                        <Link
                            className="register-login-link"
                            to="/auth/register"
                        >
                            Sign up
                        </Link>
                    </div>
                </div>
            </div>
        );
    }
}

const validate = (values) => {
    const errors = {};
    if (!values.username) {
        errors.username = "Required field";
    }
    if (!values.email) {
        errors.email = "Required field";
    } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,5}$/i.test(values.email)
    ) {
        errors.email = "Invalid email address";
    }
    if (!values.password) {
        errors.password = "Required field";
    } else if (values.password.length < 8) {
        errors.password = "Password must contain 8 or more symbols";
    }
    if (!values.password_confirm) {
        errors.password_confirm = "Required field";
    } else if (values.password !== values.password_confirm) {
        errors.password_confirm = "Passwords are not the same";
    }
    return errors;
};

Login = reduxForm({
    form: "LoginForm",
    validate,
})(Login);

const mapDispatchToProps = {
    postLogin,
};

export default connect(null, mapDispatchToProps)(Login);
